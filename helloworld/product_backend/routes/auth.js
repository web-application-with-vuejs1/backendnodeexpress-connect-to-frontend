var express = require('express');
var router = express.Router();
const User = require('../models/User')
const { generateAccessToken } = require('../helpers/auth')
const bcrypt = require('bcryptjs')

const login = async function (req, res, next) {
    const username = req.body.username
    const password = req.body.password
    try {
        const user = await User.findOne({ username: username }).exec()
        const verifyResult = await bcrypt.compare(password, user.password)
        if (!verifyResult) {
            res.status(404).json({
                message: 'User not found!!'
            })
        }
        const token = generateAccessToken({ _id: user._id, username: user.username })
        // console.log(token)
        res.json({ user: { _id: user._id, username: user.username, roles: user.roles }, token: token })
    } catch (err) {
        res.status(404).json({
            message: err.message
        })
    }
}
router.post('/login', login)
module.exports = router
